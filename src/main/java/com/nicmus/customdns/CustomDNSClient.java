/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.customdns;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nicmus.customdns.config.APIConfig;

@Component
public class CustomDNSClient {
	
	@Autowired
	private APIConfig apiConfig;

	@Autowired
	private HttpHeaders defaultHeaders;
	
	
	@Autowired
	private RestTemplate restTemplate;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String baseURL;
	
	@PostConstruct
	private void init() {
		this.baseURL = this.apiConfig.getBaseURL();
	}
		
	/**
	 * Get the zone record count for the given zone
	 * @param zoneName
	 * @return
	 */
	public int getZoneRecordCount(String zoneName) {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.path(zoneName)
				.path("/records")
				.path("/count");
		
		HttpHeaders headers = new HttpHeaders(this.defaultHeaders);
		
		ResponseEntity<String> exchange = this.restTemplate.exchange(
				uriBuilder.toUriString(), 
				HttpMethod.GET, 
				(new HttpEntity<>(headers)), 
				String.class);
	
		return Integer.parseInt(exchange.getBody());
	}
	
	/**
	 * Get the listing for the zone specified by zoneName
	 * @param zoneName the zone whose records to obtain
	 * @param offset 
	 * @param count
	 * @return the zones matching the criteria
	 */
	public List<ZoneRecord> getZoneRecords(String zoneName, int offset, int count){
			
		HttpEntity<String> httpEntity = new HttpEntity<>(this.defaultHeaders);
		UriComponentsBuilder uriComponentBuilder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.queryParam("offset", offset)
				.queryParam("count", count)
				.path(zoneName)
				.path("/records");
		
		String url = uriComponentBuilder.toUriString();
		
		ResponseEntity<List<ZoneRecord>> responseEntity = this.restTemplate.exchange(
				url, 
				HttpMethod.GET, 
				httpEntity, 
				new ParameterizedTypeReference<List<ZoneRecord>>() {});
		
		
		return responseEntity.getBody();
	}
	
	/**
	 * Get the zone records for the given zone
	 * @param zoneName
	 * @return list of ZoneRecords
	 */
	public List<ZoneRecord> getZoneRecords(String zoneName){
		
		HttpEntity<String> httpEntity = new HttpEntity<>(this.defaultHeaders);
		UriComponentsBuilder uriComponentBuilder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.path(zoneName)
				.path("/records");
		
		String url = uriComponentBuilder.toUriString();
		
		ResponseEntity<List<ZoneRecord>> responseEntity = this.restTemplate.exchange(
				url, 
				HttpMethod.GET, 
				httpEntity, 
				new ParameterizedTypeReference<List<ZoneRecord>>() {});
		
		
		return responseEntity.getBody();
	}
	
	/**
	 * Get the zone record for the given parameters
	 * @param zoneName the zonename
	 * @param id the record id
	 * @return return the zone record matching parameters
	 */
	public ZoneRecord getZoneRecord(String zoneName, int id) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.path(zoneName)
				.path("/record/")
				.path("" + id);
		
		HttpEntity<String> httpEntity = new HttpEntity<>(this.defaultHeaders);
		ResponseEntity<ZoneRecord> zoneRecordEntity = this.restTemplate.exchange(
				uriBuilder.toUriString(), 
				HttpMethod.GET, 
				httpEntity, 
				ZoneRecord.class);
		return zoneRecordEntity.getBody();
	}
	
	/**
	 * Create the given zone record
	 * @param zoneName The zonename where to add the new zone record
	 * @param zoneRecord the zonerecord to add
	 * @return the id of the zone record added
	 * @throws IOException 
	 */
	public int createZone(String zoneName, ZoneRecord zoneRecord) throws IOException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.path(zoneName);
	
		ObjectMapper mapper = new ObjectMapper();
		String jsonBody = mapper.writeValueAsString(zoneRecord);
		
		HttpHeaders headers = new HttpHeaders(this.defaultHeaders);
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<>(jsonBody,headers);
		
		ResponseEntity<String> response = this.restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, httpEntity, String.class);
		return Integer.parseInt(response.getBody());
	}
	
	/**
	 * Delete the given zone record
	 * @param zoneName the zone name the record belongs to
	 * @param recordId the record id
	 */
	public void deleteZoneRecord(String zoneName, int recordId) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.baseURL)
				.path(zoneName)
				.path("/record/")
				.path("" + recordId);
		
		HttpEntity<Void> entity = new HttpEntity<>(this.defaultHeaders);
		ResponseEntity<Void> httpResponse = this.restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, Void.class);
		this.logger.debug("Response status code {}", httpResponse.getStatusCode().toString());
	}
}
