/** *****************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************
 */
package com.nicmus.customdns.test;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nicmus.customdns.CustomDNSClient;
import com.nicmus.customdns.ZoneRecord;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomDNSTest {
    
    private Logger logger = LoggerFactory.getLogger(CustomDNSTest.class);
    
    @Autowired
    private CustomDNSClient customDNS;
    
    @Test
    public void testZoneCount() {
        final String zoneName = "nicmus.com";
        int zoneRecordCount = this.customDNS.getZoneRecordCount(zoneName);
        this.logger.debug("Zone count record count {}:{}", zoneName, zoneRecordCount);
        Assert.assertTrue(zoneRecordCount > 0);
    }
    
    @Test
    public void testZoneList() {
        String zoneName = "nicmus.com";
        
        int recordCount = this.customDNS.getZoneRecordCount(zoneName);
        Assert.assertTrue(recordCount > 0);
        
        this.logger.debug("Zone  {}:{}", zoneName, recordCount);
        
        List<ZoneRecord> zoneRecords = this.customDNS.getZoneRecords(zoneName);
        Assert.assertTrue(zoneRecords.size() == recordCount);
        
        Assert.assertTrue(recordCount > 0);
        
        //test pagination of zones loading
        int start = 0;
        final int increment=4;
        do {
            List<ZoneRecord> limitedZoneRecords = this.customDNS.getZoneRecords(zoneName, start, increment);
            this.logger.debug("Zone {}:{}:{}", limitedZoneRecords.get(0).getId(),zoneName, limitedZoneRecords.size());
            Assert.assertTrue(limitedZoneRecords.size() == increment);
            start += increment;
        } while(start < recordCount);
        
    }
    
    @Test
    public void testZone() {
        String zoneName = "nicmus.com";        
        List<ZoneRecord> zoneRecords = this.customDNS.getZoneRecords(zoneName);
        for (ZoneRecord zoneRecord : zoneRecords) {
            ZoneRecord record = this.customDNS.getZoneRecord(zoneName, zoneRecord.getId());
            Assert.assertTrue(zoneRecords.contains(record));
        }        
    }
    
    @Test
    public void testZoneCreationAndDeletion() throws IOException {
        String zoneName = "nicmus.com";
        ZoneRecord newRecord = new ZoneRecord();
        newRecord.setName("www.nicmus.com");
        newRecord.setType("A");
        newRecord.setContent("192.168.0.1");
        
        List<ZoneRecord> zoneRecords = this.customDNS.getZoneRecords(zoneName);
        
        int createZone = this.customDNS.createZone(zoneName, newRecord);
        
        Assert.assertTrue(!zoneRecords.contains(newRecord));
        this.logger.info("New record id: {}", createZone);
        Assert.assertTrue(createZone > 0);
        
        List<ZoneRecord> newRecords = this.customDNS.getZoneRecords(zoneName);
        ZoneRecord addedRecord = this.customDNS.getZoneRecord(zoneName, createZone);
        Assert.assertTrue(newRecords.contains(addedRecord));
        
        this.customDNS.deleteZoneRecord(zoneName, createZone);
        
        List<ZoneRecord> zoneRecordsAfterRemoval = this.customDNS.getZoneRecords(zoneName);
        Assert.assertTrue(!zoneRecordsAfterRemoval.contains(addedRecord));
    }
    
}
